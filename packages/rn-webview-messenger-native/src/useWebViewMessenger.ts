import { MutableRefObject, useCallback, useRef, useState } from 'react';
import { WebViewMessenger, WebViewMessengerProps } from './WebViewMessenger';
import { CallMethodFn } from '@proscom/rn-webview-messenger-common';
import WebView, { WebViewProps } from 'react-native-webview';

export function useWebViewMessenger({
  callMethod,
  appVersion,
  info,
  webviewRef,
  messengerConfig
}: {
  callMethod: CallMethodFn;
  appVersion: string;
  info?: any;
  webviewRef?: MutableRefObject<WebView>;
  messengerConfig?: Partial<WebViewMessengerProps>
}): [WebViewMessenger, () => Partial<WebViewProps>] {
  // Эта функция выполняет js код code внутри webview
  // Через нее организуется доставка сообщений из натива в веб
  const injectJavaScript = useCallback((code: string) => {
    webviewRef.current?.injectJavaScript(code);
  }, []);

  const [messenger, setMessenger] = useState<WebViewMessenger>(undefined);
  messenger?.update({
    callMethod,
    injectJavaScript
  });

  const lastMessengerRef = useRef<WebViewMessenger>(messenger);
  lastMessengerRef.current = messenger;

  const handleLoadStart = useCallback(() => {
    // Очищаем висящие запросы в старом messenger
    lastMessengerRef.current?.destroy();

    // Создаем новый messenger при перезагрузке страницы
    setMessenger(
      new WebViewMessenger({
        ...messengerConfig,
        callMethod,
        appVersion,
        info,
        injectJavaScript,
      })
    );
  }, [callMethod, injectJavaScript, info, messengerConfig]);

  const getWebViewProps = () => ({
    ...messenger?.getWebviewProps(),
    onLoadStart: handleLoadStart
  });

  return [
    messenger,
    getWebViewProps
  ];
}
