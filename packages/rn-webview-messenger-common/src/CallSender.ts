import { CallMethodMessage, CallResultMessage, MessageTypes } from './types';

export interface CallMetadata {
  callId: number;
  resolve: (result?: any) => void;
  reject: (error?: any) => void;
  timeoutId?: number;
}

export type SendMessageFn = (message: CallMethodMessage) => void;

export class CallSender {
  defaultCallTimeout: number = null;
  lastCallId: number = 0;
  calls: { [id: number]: CallMetadata } = {};
  sendMessage: SendMessageFn;

  constructor({
    sendMessage,
    defaultCallTimeout
  }: {
    sendMessage: SendMessageFn;
    defaultCallTimeout?: number;
  }) {
    this.sendMessage = sendMessage;
    this.defaultCallTimeout = defaultCallTimeout;
  }

  receiveResult = (message: CallResultMessage) => {
    const { result, error, callId } = message;
    const callInfo = this.calls[callId];
    if (!callInfo) return;

    clearTimeout(callInfo.timeoutId);

    if (error) {
      callInfo.reject(error);
    } else {
      callInfo.resolve(result);
    }
  }

  callTimed = <Args extends any[]>(methodName: string, timeout: null | number, ...args: Args) => {
    this.lastCallId++;
    const callId = this.lastCallId;

    let resolvePromise = null;
    let rejectPromise = null;
    const resultPromise = new Promise((resolve, reject) => {
      resolvePromise = resolve;
      rejectPromise = reject;
    });

    this.calls[callId] = {
      callId,
      resolve: resolvePromise,
      reject: rejectPromise,
      timeoutId: timeout
        ? setTimeout(() => {
            rejectPromise('timeout');
          }, timeout)
        : null
    };

    const message: CallMethodMessage = {
      type: MessageTypes.CALL_METHOD,
      callId,
      methodName,
      args
    };
    this.sendMessage(message);

    return resultPromise;
  }

  call = <Args extends any[]>(methodName: string, ...args: Args) => {
    return this.callTimed(methodName, this.defaultCallTimeout, ...args);
  }
}
