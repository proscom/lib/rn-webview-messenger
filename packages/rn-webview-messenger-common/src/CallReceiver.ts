import { CallMethodMessage, CallResultMessage, MessageTypes } from './types';

export type CallMethodFn = (methodName: string, args: any[]) => any | Promise<any>;
export type SendResponseFn = (result: CallResultMessage, callMessage: CallMethodMessage) => void;

export class CallReceiver {
  callMethod: CallMethodFn;
  sendResponse: SendResponseFn;

  constructor({ callMethod, sendResponse }: { callMethod: CallMethodFn, sendResponse: SendResponseFn }) {
    this.callMethod = callMethod;
    this.sendResponse = sendResponse;
  }

  receiveCall(message: CallMethodMessage) {
    const { methodName, args, callId } = message;

    const resolve = (result: any) => {
      const response: CallResultMessage = {
        type: MessageTypes.METHOD_RESULT,
        callId,
        result
      };
      this.sendResponse(response, message);
    };

    const reject = (error: any) => {
      const response: CallResultMessage = {
        type: MessageTypes.METHOD_RESULT,
        callId,
        error
      };
      this.sendResponse(response, message);
    };

    Promise.resolve()
      .then(() => this.callMethod(methodName, args))
      .then((value) => resolve(value))
      .catch((error) => reject(error));
  }
}
