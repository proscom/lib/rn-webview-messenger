export type ReadinessListener = {
  success?: () => void;
  error?: (err: any) => void;
};

export class ReadyState {
  ready: boolean = false;
  error: any = null;
  listeners: ReadinessListener[] = [];

  subscribe(listener: ReadinessListener) {
    if (this.ready) {
      if (listener.success) {
        listener.success();
      }
    } else if (this.error) {
      if (listener.error) {
        listener.error(this.error);
      }
    } else {
      this.listeners.push(listener);
    }
  }

  unsubscribe(listener: ReadinessListener) {
    const index = this.listeners.indexOf(listener);
    if (index >= 0) {
      this.listeners.splice(index, 1);
    }
  }

  setReady() {
    if (this.ready || this.error) return;
    this.ready = true;
    for (const listener of this.listeners) {
      if (listener.success) {
        listener.success();
      }
    }
    this.listeners.length = 0;
  }

  setError(error: any) {
    if (this.ready || this.error) return;
    this.error = error;
    for (const listener of this.listeners) {
      if (listener.success) {
        listener.error(error);
      }
    }
    this.listeners.length = 0;
  }

  getPromise() {
    return new Promise<undefined>((success, error) => {
      this.subscribe({
        success: () => success(undefined),
        error
      });
    });
  }

  then(callback: () => void) {
    return this.getPromise().then(callback);
  }

  catch(callback: (err: any) => void) {
    return this.getPromise().catch(callback);
  }
}
