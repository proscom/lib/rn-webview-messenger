export interface NativeMessage {
  type?: string;
}

export enum MessageTypes {
  NATIVE_HANDSHAKE = 'nativeHandshake',
  WEB_HANDSHAKE = 'webHandshake',
  CALL_METHOD = 'callMethod',
  METHOD_RESULT = 'methodResult'
}

export interface NativeHandshakeMessage extends NativeMessage {
  type: MessageTypes.NATIVE_HANDSHAKE;
  appVersion?: string;
  appPlatform?: string;
  nativeMessengerVersion?: string;
  info?: any;
}

export interface CallMethodMessage extends NativeMessage {
  type: MessageTypes.CALL_METHOD;
  callId?: number;
  methodName?: string;
  args?: any[];
  callback?: string;
}

export interface CallResultMessage extends NativeMessage {
  type: MessageTypes.METHOD_RESULT;
  callId?: number;
  result?: any;
  error?: any;
}

export interface WebHandshakeMessage extends NativeMessage {
  type: MessageTypes.WEB_HANDSHAKE;
  webVersion?: string;
  webInfo?: any;
  webMessengerVersion?: string;
  defaultCallback?: string;
}

export type LogFn = (...args: any[]) => void;
