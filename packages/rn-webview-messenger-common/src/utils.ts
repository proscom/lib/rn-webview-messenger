import { NativeMessage } from './types';

export function tryParseJson(data: string | null | undefined) {
  try {
    return JSON.parse(data) as unknown;
  } catch (e) {
    return null;
  }
}

export function isNativeMessage(obj: any): obj is NativeMessage {
  return obj && typeof obj === 'object' && obj['type'];
}
